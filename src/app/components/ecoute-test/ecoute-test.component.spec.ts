import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EcouteTestComponent } from './ecoute-test.component';

describe('EcouteTestComponent', () => {
  let component: EcouteTestComponent;
  let fixture: ComponentFixture<EcouteTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EcouteTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EcouteTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
