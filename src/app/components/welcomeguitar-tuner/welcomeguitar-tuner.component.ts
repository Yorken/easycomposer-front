import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { Utilisateur } from 'src/app/modele/user';

@Component({
  selector: 'app-welcomeguitar-tuner',
  templateUrl: './welcomeguitar-tuner.component.html',
  styleUrls: ['./welcomeguitar-tuner.component.scss']
})
export class WelcomeguitarTunerComponent implements OnInit {

  idUser: string;
  UserConnected: Utilisateur;
  usernameUserConnected: string; 

  constructor(private userService: UserService,
              private route: Router) { }

  ngOnInit() {
    //Récuperd l'id dans le local storage 
    let idUser = localStorage.getItem('id');
    this.userService.getInfoUser(idUser).subscribe(
      data => {
        this.UserConnected = new Utilisateur();
        this.UserConnected = data as Utilisateur;
        //Ecris dynamiquement le nom d'utilisateur
        this.usernameUserConnected = this.UserConnected.username;
        
      });
  }

  redirectGuitarTuner(){
    this.route.navigate(['/guitar-tuner/']);
  }

  redirectHome(){
    this.route.navigate(['/home/']);
  }

}
