import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { RegisterService } from 'src/app/services/register.service';
import { sha256 } from 'js-sha256';
import { Utilisateur } from 'src/app/modele/user';
import { MessageService } from 'src/app/services/message.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-form-register',
  templateUrl: './form-register.component.html',
  styleUrls: ['./form-register.component.scss']
})
export class FormRegisterComponent implements OnInit {

  registerForm: FormGroup;
  submitted=false;

  //User
  user: Utilisateur;

  //Boolean pour afficher message d'erreur 
  errorCreateUser: boolean = false;
  successCreateUser: boolean = false;

  constructor(private formBuilder: FormBuilder,
     private serviceRegister: RegisterService,
     private messageService: MessageService,
     private route: Router) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)], Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$')],
      confirmPassword: ['', [Validators.required, Validators.minLength(8)]],
      username: ['', Validators.required]
    }, {
      validator: this.serviceRegister.concordanceMdp('password', 'confirmPassword')
  });

  } 

  get formField(){
    return this.registerForm.controls;
  }

  onSubmit(){
    this.submitted = true;
    //On arrête la fonction si le formulaire n'est pas remplit correctement
    if(this.registerForm.invalid){
      return;
    }else{    
    //Création de notre user avec les informations reprise du formulaire
    this.user = new Utilisateur();
    this.user.email = this.registerForm.get('email').value;
    this.user.password = sha256(this.registerForm.get('password').value);
    this.user.username = this.registerForm.get('username').value;;
    //Soumet au service les données du formulaire 
    this.serviceRegister.creerUser(this.user).subscribe(
      response => this.messageSucess(),
      err => this.messageError()
    );
  }

}

messageSucess(){
  this.successCreateUser=true;
  this.errorCreateUser=false;
  setTimeout(() => {
    this.route.navigate(['/login']);
  }, 5000);
}

messageError(){
  this.successCreateUser=false;
  this.errorCreateUser=true;
  this.resetForm();
}

resetForm(){
  //Réinitialise le formulaire d'enregistrement à chaque tentative. 
  this.registerForm.reset();
}
  
}
