import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api/menuitem';
import {MessageService} from 'primeng/api';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    isConnected:boolean =false;
  constructor(private messageService: MessageService,
            private route: Router,
            private userService: UserService) { }

  items: MenuItem[];
  
  ngOnInit() {
  
      this.items = [
          {
              label: 'Mon Compte',
              icon: 'pi pi-list',
              items: [{
                      label: 'Mes informations', 
                      routerLink: ['/my-account'],
                      icon: 'pi pi-user-edit',
                      
                  },
                    ]
          },
          {
            label: 'Home',
            routerLink: ['/home'],
            icon: 'pi pi-home',
        },
          {
              label: 'Guitar Tuner',
              routerLink: ['/guitar-tuner'],
              icon: 'pi pi-sliders-v',
          },
          {
              label: 'Informations',
              routerLink: ['/informations'],
              icon: 'pi pi-info-circle',
          },
          {separator:true},
          {
              label: 'Quit', 
              routerLink: ['/home'],
              icon: 'pi pi-fw pi-times',
          },
          {separator:true},
        //   { 
        //     label: 'Easy Composer',

        //   }
      ];

  }

  showConfirm(){
    this.messageService.clear();
    this.messageService.add({key: 'c', sticky: true, severity:'warn', summary:'Etes-vous sur de vouloir vous déconnecter ?', detail:'Validez pour continuer'});
  }

  onConfirm() {
    this.messageService.clear('c');
    this.userService.signOut();
}

onReject() {
    this.messageService.clear('c');
    
}

}
