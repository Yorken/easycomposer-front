import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuitarTabInformationsComponent } from './guitar-tab-informations.component';

describe('GuitarTabInformationsComponent', () => {
  let component: GuitarTabInformationsComponent;
  let fixture: ComponentFixture<GuitarTabInformationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuitarTabInformationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuitarTabInformationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
