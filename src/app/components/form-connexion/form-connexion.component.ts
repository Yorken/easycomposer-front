import { Component, OnInit } from '@angular/core';
import {MessageService} from 'primeng/api';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { LoginService } from 'src/app/services/login.service';
import { sha256 } from 'js-sha256';
import { Utilisateur } from 'src/app/modele/user';
import { Router } from '@angular/router';


@Component({
  selector: 'app-form-connexion',
  templateUrl: './form-connexion.component.html',
  styleUrls: ['./form-connexion.component.scss'],
  providers: [MessageService]
})
export class FormConnexionComponent implements OnInit {
  
  loginForm: FormGroup;

  error:boolean = false;
  errorUserAndPassword:boolean=false;

  userRecuperer: Utilisateur; 
  idUserConnecte:any;
  constructor(
    private formBuilder: FormBuilder,
    private loginService: LoginService,
    private route: Router
  ) { }
  

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
        email: ['', [Validators.required, Validators.email]],
        password: ['', Validators.required]
    });
   
  
}

onSubmit(){
  //Vérifie que le formulaire est valide 
  if(this.loginForm.invalid){
    this.error = true;
    return;
  }else{   
    //Chiffrement du mot de passe saisie dans le formulaire
    let passwordSaisieHashed = sha256(this.loginForm.get('password').value);
    //vérifie si l'adresse mail est présente dans la BDD 
    this.loginService.verifierId(this.loginForm.get('email').value).subscribe(
      data => {
        //Un utilisateur a été trouvé 
        //on créer un objet utilisateur
        this.userRecuperer = new Utilisateur();
        this.userRecuperer = data as Utilisateur;
        //console.log(this.userRecuperer);
        //console.log("password recup :" + this.userRecuperer[0].password);
        //console.log(passwordSaisieHashed);
        //Vérifie que les mots de passent concorde 
        if(this.userRecuperer[0].password === passwordSaisieHashed){
          //authentification SUCCESS
          //Place l'id user dans le localstorage 
          localStorage.setItem('id', this.userRecuperer[0].id );
          this.route.navigate(['/welcome']); 
        }else{
          //Mot de passe erroné 
          this.errorUserAndPassword = true; 
          this.resetForm();
        }

      },
      err => this.error = true
    );
  } 
}

resetForm(){
  this.loginForm.reset();
}

}
