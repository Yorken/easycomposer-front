import { Component, OnInit } from '@angular/core';
import {MessageService} from 'primeng/api';
import {SelectItem} from 'primeng/api';
import { UserService } from 'src/app/services/user.service';
import { Utilisateur } from 'src/app/modele/user';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RegisterService } from 'src/app/services/register.service';
import { sha256 } from 'js-sha256';

@Component({
  selector: 'app-form-account-setting',
  templateUrl: './form-account-setting.component.html',
  styleUrls: ['./form-account-setting.component.scss']
})
export class FormAccountSettingComponent implements OnInit {
  note: number;
  sexe: SelectItem[];
  age: SelectItem[];
  experience: SelectItem[];

  //Variable pour afficher les informations connus 
  usernameUserConnected: string;
  emailActuel: string;
  UserConnected: Utilisateur;

  //formgroup 
  //changer adresse mail : 
  changeEmailForm: FormGroup;
  errorNewEmail:boolean = false;
  newEmailSuccess:boolean=false;
  activerModifEmail:boolean = false;

  //changer password
  changePasswordForm: FormGroup;
  errorNewPassword:boolean = false;
  succesPasswordChange:boolean = false;
  activerModifPassword:boolean = false;

  //changer username
  changeUsernameForm: FormGroup;
  errorUsernameChange:boolean = false;
  successUsernameChange:boolean = false;
  activerModifUsername:boolean = false;

  //post feedback
  postFeedbackForm: FormGroup;
  postFeedbackSuccess:boolean = false;
  postFeedbackFailed:boolean = false;

  //delete account
  checkboxDeleteChecked:boolean = false; 
  errorCheckedDeleteAccount:boolean = false;
  errorDelete:boolean = false;

  constructor(private messageService: MessageService,
              private userService: UserService,
              private formBuilder: FormBuilder,
              private serviceRegister: RegisterService,
              private route: Router) {}
   
  
  ngOnInit() {
    //Récuperd l'id dans le local storage 
    let idUser = localStorage.getItem('id');
    this.userService.getInfoUser(idUser).subscribe(
      data => {
        this.UserConnected = new Utilisateur();
        this.UserConnected = data as Utilisateur;
        //Ecrit dynamiquement le nom d'utilisateur dans le component 
        this.usernameUserConnected = this.UserConnected.username;
        //Rempli le champs e-mail actuel 
        this.emailActuel = this.UserConnected.email;
        
      });
    
    //Initilisation des formGroup
    //** changeEmailForm: FormGroup */
    this.changeEmailForm = this.formBuilder.group({
      newEmail: [{value: '', disabled: true },[Validators.required, Validators.email]]
    });

     //** changePasswordForm: FormGroup */
    this.changePasswordForm = this.formBuilder.group({
      passwordActuel: [{value: '', disabled: true }, [Validators.required, Validators.minLength(8)]],
      newPassword: [{value: '', disabled: true }, [Validators.required, Validators.minLength(8)], Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$')],
      newPasswordConfirmed: [{value: '', disabled: true }, [Validators.required, Validators.minLength(8)]],
      }, {
        validator: this.serviceRegister.concordanceMdp('newPassword', 'newPasswordConfirmed')
    });

    //** changeUsernameForm: FormGroup */
    this.changeUsernameForm = this.formBuilder.group({
      newUsername: [{value: '', disabled: true}, Validators.required]
    });

    //** postFeedbackForm: FormGroup */
    this.postFeedbackForm = this.formBuilder.group({
      noteFeedback: [''],
      sexeFeedback: [''],
      ageFeedback: [''],
      experienceFeedback: [''],
      avisFeedback: ['']
    });

    //Initialisation des dropdowns
    this.sexe = [];
    this.sexe.push({label: 'Selectionner votre sexe', value:''});
    this.sexe.push({label: 'Femme', value:'F'});
    this.sexe.push({label: 'Homme', value:'H'});

    this.age = [];
    this.age.push({label: 'Selectionner votre tranche d\'âge', value:''});
    this.age.push({label: '0-14 ans', value:'0-14 ans'});
    this.age.push({label: '15-29 ans', value:'15-29 ans'});
    this.age.push({label: '30-44 ans', value:'30-44 ans'});
    this.age.push({label: '45-59 ans', value:'45-59 ans'});
    this.age.push({label: '60 et plus', value:'60 et plus'});

    this.experience = [];
    this.experience.push({label: 'Votre expérience dans la musique', value:''});
    this.experience.push({label: 'Débutant (< 1 ans)', value:'debutant'});
    this.experience.push({label: 'Confirmé (2-5 ans)', value:'confirme'});
    this.experience.push({label: 'Expert (> 5 ans)', value:'expert'});
    
  }

  activerFormEmail(){
    if(this.activerModifEmail){
      this.activerModifEmail = !this.activerModifEmail;
      this.changeEmailForm.get('newEmail').disable();
      
    }else{
      this.activerModifEmail = true;
      this.changeEmailForm.get('newEmail').enable();
    }
    
  }

  changerEmail(){
    if(this.changeEmailForm.invalid){
      this.errorNewEmail = true;
      return;
    }else{
      //changer l'adresse mail 
      //Vérifie que l'adresse mail n'est pas identique
      if(this.emailActuel != this.changeEmailForm.get('newEmail').value){
          //Recuperd le nouvel e-mail saisie dans le form
          this.userService.changerEmail(this.changeEmailForm.get('newEmail').value).subscribe(
            response => this.changerEmailSuccess(),
            err => this.changerEmailError()
          );
          this.changeEmailForm.reset();
      }else{
        this.errorNewEmail = true;
        return;
      }
      
    }
  }

  activerFormPassword(){
    if(this.activerModifPassword){
      this.activerModifPassword = !this.activerModifPassword;
      this.changePasswordForm.get('passwordActuel').disable();
      this.changePasswordForm.get('newPassword').disable();
      this.changePasswordForm.get('newPasswordConfirmed').disable();
    }else{
      this.activerModifPassword = true;
      this.changePasswordForm.get('passwordActuel').enable();
      this.changePasswordForm.get('newPassword').enable();
      this.changePasswordForm.get('newPasswordConfirmed').enable();
    }
  }

  changerEmailSuccess(){
    //Reset l'input newEmail. 
    this.changeEmailForm.reset();
    this.errorNewEmail = false;
    this.newEmailSuccess = true;
    //Rafraichis le component pour actualiser les données 
    this.ngOnInit();
  }

  changerEmailError(){
    this.errorNewEmail = true;
    this.newEmailSuccess = false;
  }

  changePassword(){
    if(this.changePasswordForm.invalid){
      this.errorNewPassword = true;
      console.log("Form invalide");
      return;
    }else{
      this.errorNewPassword = false;
      this.succesPasswordChange = true;
      console.log("Form valide");
      // console.log(this.changePasswordForm.get('newPassword').value);
      console.log("Saisie : " + sha256(this.changePasswordForm.get('passwordActuel').value));
      //To do change password
      //vérifie que le mot de passe actuel saisie est correcte 
      console.log("User : " + this.UserConnected.password);

      if(this.UserConnected.password === sha256(this.changePasswordForm.get('passwordActuel').value)){
        // console.log(this.changePasswordForm.get('newPassword').value);
        // console.log(sha256(this.changePasswordForm.get('newPassword').value));
        console.log('Mot de passe ok, je suis avant d appeler mon service');
        this.userService.changerPassword(sha256(this.changePasswordForm.get('newPassword').value)).subscribe(
          response => this.changerPasswordSuccess(),
          err => this.changerPasswordError()
        );
        this.changePasswordForm.reset();
      }else{
        this.changerPasswordError()
        this.changePasswordForm.reset()
        return;
      }
      
    }
  }

  changerPasswordSuccess(){
    //console.log('success');
    this.errorNewPassword = false;
    this.succesPasswordChange = true;
  }

  changerPasswordError(){
    //console.log('You fail')
    this.errorNewPassword = true;
    this.succesPasswordChange = false;
  }

  activerFormUsername(){
    if(this.activerModifUsername){
      this.activerModifUsername = !this.activerModifUsername;
      this.changeUsernameForm.get('newUsername').disable();
      
    }else{
      this.activerModifUsername = true;
      this.changeUsernameForm.get('newUsername').enable();
    }
  }
  
  changeUsername(){
    if(this.changeUsernameForm.invalid){
      this.errorUsernameChange = true;
      return;
    }else{
      //Si le nom d'utilisateur est différent de celui actuel.
      if(this.usernameUserConnected != this.changeUsernameForm.get('newUsername').value){
        //Recuperd le nouvel e-mail saisie dans le form
        this.userService.changerUsername(this.changeUsernameForm.get('newUsername').value).subscribe(
        response => this.changerUsernameSuccess(),
        err => this.changerUsernameError()
      );
        this.changeUsernameForm.reset();
      }else{
        this.errorUsernameChange = true;
        return;
      }
    } 
}

  changerUsernameError(){
    //Reset l'input newEmail. 
    this.changeEmailForm.reset();
    //Rafraichis le component pour actualiser les données 
    this.errorUsernameChange = true;
    this.successUsernameChange = false;
    
  }

  changerUsernameSuccess(){
    //Reset l'input newEmail. 
    this.changeEmailForm.reset();
    this.errorUsernameChange = false;
    this.successUsernameChange = true;
    this.ngOnInit();
  }

  


  postFeeback(){
    if(this.postFeedbackForm.invalid){
      this.postFeedbackFailed = true;
      return;
    }else{
      //todo post feedback
      //Recuperd les champs du formulaires 
      if(this.note != null){
        //Une note a été donné 
        //Recupérer le sexe 
        let sexe = this.postFeedbackForm.get('sexeFeedback').value;
        //console.log(this.postFeedbackForm.get('sexeFeedback').value);
        //Récuperd la tranche d'âge 
        let trancheAge = this.postFeedbackForm.get('ageFeedback').value;
        //console.log(this.postFeedbackForm.get('ageFeedback').value);
        //Récuperd l'expérience 
        let experience = this.postFeedbackForm.get('experienceFeedback').value;
        //console.log(this.postFeedbackForm.get('experienceFeedback').value);
        //Récuperd le commentaire
        let commentaire = this.postFeedbackForm.get('avisFeedback').value;
        //console.log(this.postFeedbackForm.get('avisFeedback').value);
        this.userService.giveFeedback(this.note, sexe, trancheAge, experience,commentaire).subscribe(
          response => this.feedbackSuccess(),
          err => this.feedbackError()
        );
        this.feedbackSuccess();
      }else{
        //Aucune note a été donnée 
        this.feedbackError();
      }
    }
  }

  feedbackSuccess(){
    this.postFeedbackSuccess = true;
    this.postFeedbackFailed = false;
  }

  feedbackError(){
    this.postFeedbackSuccess = false;
    this.postFeedbackFailed = true;
  }

  supprimerCompte(){
    if(this.checkboxDeleteChecked){
      //la checkbox a bien été coché  
      this.errorCheckedDeleteAccount = false;
            
      this.userService.deleteAccount().subscribe((data)=>{
        this.deleteSuccessAccount(),
        (err)=> this.deleteErrorAccount();
      });
      
    }else{
      this.errorCheckedDeleteAccount = true;
    }
   
  }

  deleteSuccessAccount(){
    //suppression de l'id du local storage. 
    localStorage.removeItem('id');
    this.route.navigate(['/login']);
  }

  deleteErrorAccount(){
    this.errorDelete = true; 
  }

}
