import { Component, OnInit, NgZone } from "@angular/core";
import { ServiceNoteService } from "src/app/services/service-note.service";
import { Note } from "src/app/modele/note";
import { MessageService, Message } from "primeng/api";
import { EcouteService } from "src/app/services/ecoute.service";
import { SelectItem } from "primeng/api";
import { Historique } from "src/app/modele/historique";
import { interval, Subscription } from "rxjs";

@Component({
  selector: "app-guitar-tab",
  templateUrl: "./guitar-tab.component.html",
  styleUrls: ["./guitar-tab.component.scss"]
})
export class GuitarTabComponent implements OnInit {
  noteSelect = "Cliquez sur une note";
  noteDetected;
  frequenceDetected = "--- Hz";
  historiques: Historique[] = [];
  historique: Historique;
  noteString: string;
  isActivated: boolean = true;
  //tableau qui contiendra les numeros de note permettant de réaliser l'affichage
  numeroNoteProposer: string[];
  // valeur a sauvegarder
  gammeAEnevoyer = "";
  noteAEnvoyer: string;
  gammes: SelectItem[];
  selectGamme: any;
  tabNoteBoolean = new Array<boolean>(45);
  indexNote: number;
  public listNotes: Note[];
  frequency;
  noteDetectedEcoute;
  noteRecu: Note;
  audioActivated: boolean = false;
  //argument test
  error = false;
  msgs: Message[] = [];
  ctx = new AudioContext();
  numeroProposition = 0;
  subscription: Subscription;

  constructor(
    private service: ServiceNoteService,
    private notes: Note,
    private messageService: MessageService,
    private zone: NgZone,
    private serviceEcoute: EcouteService
  ) {}

  ngOnInit() {
    // this.displayFrequence()
    // this.listen();
    this.gammes = [];
    this.gammes.push({ label: "Choisissez votre gamme", value: "" });
    this.gammes.push({ label: "Majeur", value: "major" });
    this.gammes.push({ label: "Mineur", value: "minor" });
    this.gammes.push({ label: "Pentatonique Mineure", value: "pentaMinor" });
    this.gammes.push({ label: "Pentatonique Majeure", value: "pentaMajor" });
    // document.getElementById('ecoute').addEventListener("click", this.listen(){
    //   document.getElementById("demo").innerHTML = "Hello World";
    // });
  }

  recupererNote(note: string) {
    //Mise à jour de "noteSelect"
    var reg = new RegExp("[0-9]");
    this.noteSelect = note.replace(reg, "");

    //Transmet la demande a recupererNoteService
    //transmet la note au service
    this.service.recupererNoteService(note);
    this.noteAEnvoyer = note;
  }

  selectedGamme(event) {
    //Recuperd la value du dropdown
    this.gammeAEnevoyer = event.value;
  }

  onSoumettreBack() {
    this.resetFret(this.listNotes);
    this.initTableau();
    //console.log(this.selectedGamme);
    this.service.soumettreBackService(this.gammeAEnevoyer).subscribe(data => {
      //On récuperd les données du back sous forme de JSON
      this.listNotes = data as Note[]; // MAj du component
      //reset la liste de noteString
      this.noteString = "";
      //Récuperd le nom de chaque note
      for (let i = 0; i < this.listNotes.length; i++) {
        this.noteString += " " + this.listNotes[i].nom;
      }
      console.log(this.noteString);
      this.majFret(this.listNotes);
      this.afficherHistorique(
        this.noteSelect,
        this.gammeAEnevoyer,
        this.noteString
      );
    });
  }

  initTableau() {
    //Initialise le tableau de notes a chaque fois qu'on soumet une note
    for (let i = 0; i < 46; i++) {
      this.tabNoteBoolean[i] = false;
    }
  }

  majFret(listNotes: Note[]) {
    let elements;
    // On vérifie si l'id de la note reçue est présent dans le tableau de booleens si oui la note s'allume
    for (let i = 0; i < this.tabNoteBoolean.length; i++) {
      for (let j = 0; j < listNotes.length; j++) {
        if (i === listNotes[j].demiTons) {
          this.tabNoteBoolean[i] = true;
          // pour chaque note elles font toutes partie d'une htmlCollection (un tableau) entré dans elements
          elements = document.getElementsByClassName(this.listNotes[j].nom);
          for (let x = 0; x < elements.length; x++) { // le for i est necessaire sur un HTMLcollection
            document
              .getElementById(elements[x].id)
              .classList.add("notePropose");
          }
        }

        // document.getElementById(this.listNotes[j].nom).classList.add('notePropose');
      }
    }
  }

  resetFret(listNotes: Note[]) {
    let elements;
    if (listNotes != null) {
      for (let i = 0; i < listNotes.length; i++) {
        elements = document.getElementsByClassName(this.listNotes[i].nom);
        for (let x = 0; x < elements.length; x++) {
          document
            .getElementById(elements[x].id)
            .classList.remove("notePropose");
        }
      }
    }
  }

  listen() {
    if (this.audioActivated == true) {
      navigator.getUserMedia = navigator.getUserMedia;
      const callback = stream => {
        let context = this.ctx;
        var mic = context.createMediaStreamSource(stream);
        var analyser = context.createAnalyser();
        mic.connect(analyser);
        // osc.start(0);
        var data = new Uint8Array(analyser.frequencyBinCount);
        analyser.minDecibels = -45;

        const play = () => {
          analyser.getByteFrequencyData(data);
          // get fullest bin
          var idx = 0;
          for (var j = 0; j < analyser.frequencyBinCount; j++) {
            if (data[j] > data[idx]) {
              idx = j;
            }
          }
          let a = (idx * context.sampleRate) / analyser.fftSize; // surement ici qu'on définie la frequence
          if (a > 0){
          this.frequency = a;
          }
          setTimeout(play, 1000);
        };
        play();
      };

      navigator.getUserMedia(
        // recupère video et micro si true
        { video: false, audio: true },
        callback,
        console.log
      );
    }
  }

  requeteListen() {
    //Ma partie :
    if (this.audioActivated == true) {
      this.resetFret(this.listNotes); // si on enlève cette ligne le manche devient un tetris hardcore musical
      if (this.frequency > 80 && this.frequency < 1150) {
        this.serviceEcoute
          .getGammeByFrequence(this.gammeAEnevoyer, this.frequency)
          .subscribe(data => {
            this.listNotes = data as Note[]; // MAj du component
            //Récuperd le nom de chaque note
            for (let i = 0; i < this.listNotes.length; i++) {
              this.noteString += " " + this.listNotes[i].nom;
            }
            this.noteDetectedEcoute = this.listNotes[0].nom;
            this.afficherHistorique(
              this.noteDetectedEcoute,
              this.gammeAEnevoyer,
              this.noteString
            );
            console.log(this.listNotes);
            this.majFret(this.listNotes);
          });
      }
    }
  }

  activeListen() {
    this.ctx.resume();
    const source = interval(3100);
    this.isActivated = false;
    this.audioActivated = true;
    this.messageService.add({
      severity: "success",
      summary: "Information :",
      detail: "Micro Activé !"
    });
    this.subscription = source.subscribe(val => {
      this.listen();
      this.requeteListen();
    });
  }

  mute() {
    //Le micro n'écoute plus
    this.isActivated = true;
    this.audioActivated = false;
    this.messageService.add({
      severity: "info",
      summary: "Information :",
      detail: "Micro désactivé !"
    });
    this.ctx.suspend();
    //reset la fret
    this.initTableau();
    // this.resetFret(this.listNotes);
  }

  onReject() {
    this.messageService.clear("c");
  }

  clear() {
    this.messageService.clear();
  }

  afficherHistorique(note: string, gamme: string, liste: string) {
    this.numeroProposition++;
    this.historique = new Historique();
    this.historique.index = this.numeroProposition;
    this.historique.note = note;
    this.historique.gamme = gamme;
    this.historique.listeNotes = liste;
    this.historiques.push(this.historique);
  }
}
