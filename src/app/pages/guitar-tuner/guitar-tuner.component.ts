import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import { MenuItem } from 'primeng/api/menuitem';
import {MessageService} from 'primeng/api';
import { Router } from '@angular/router';

@Component({
  selector: "app-guitar-tuner",
  templateUrl: "./guitar-tuner.component.html",
  styleUrls: ["./guitar-tuner.component.scss"],
  styles: [`
        .ui-steps .ui-steps-item {
            width: 14%;
        }

        .ui-steps.steps-custom {
            margin-bottom: 30px;
        }

        .ui-steps.steps-custom .ui-steps-item .ui-menuitem-link {
            padding: 0 1em;
            overflow: visible;
        }

        .ui-steps.steps-custom .ui-steps-item .ui-steps-number {
            background-color: #0081c2;
            color: #FFFFFF;
            display: inline-block;
            width: 36px;
            border-radius: 50%;
            margin-top: -14px;
            margin-bottom: 10px;
        }

        .ui-steps.steps-custom .ui-steps-item .ui-steps-title {
            color: #555555;
        }
    `],
    encapsulation: ViewEncapsulation.None
})
export class GuitarTunerComponent implements OnInit {

  items: MenuItem[];
  activeIndex: number = 0;
  step1:boolean = true;
  step1Ok:boolean = false;
  step2:boolean = false;
  step2Ok:boolean = false;
  step3:boolean = false;
  step3Ok:boolean = false;
  step4:boolean = false;
  step4Ok:boolean = false;
  step5:boolean = false;
  step5Ok:boolean = false;
  step6:boolean = false;
  step6Ok:boolean = false;
  accordagefinit:boolean = false;
  simulationFreq:number;
  
    //frequency;
  //colorSet = new am4core.ColorSet();
  colorSet = [
    am4core.color("#511224"),
    am4core.color("#2E53A0"),
    am4core.color("#4B7A8E"),
    am4core.color("#9E8D88"),
    am4core.color("#EBA788"),
    am4core.color("#973027")
  ];

  constructor(
    private messageService: MessageService,
    private route: Router,
  ) {}

  ngOnInit() {
    this.items = [{
      label: 'Mi 82.40 Hz',
      command: (event: any) => {
          this.activeIndex = 0;
          this.messageService.add({severity:'info', summary:'Step 1', detail: event.item.label});
      }
  },
  { 
      label: 'La 110Hz ',
      command: (event: any) => {
          this.activeIndex = 1;
          this.messageService.add({severity:'info', summary:'Step 2', detail: event.item.label});
      }
  },
  {
      label: 'Ré 146.83 Hz',
      command: (event: any) => {
          this.activeIndex = 2;
          this.messageService.add({severity:'info', summary:'Step 3', detail: event.item.label});
      }
  },
  {
      label: 'Sol 196 Hz',
      command: (event: any) => {
          this.activeIndex = 3;
          this.messageService.add({severity:'info', summary:'Step 4', detail: event.item.label});
      }
  },
  {
    label: 'Si 246.94 Hz',
    command: (event: any) => {
        this.activeIndex = 4;
        this.messageService.add({severity:'info', summary:'Step 5', detail: event.item.label});
    }
},
{
  label: 'Mi Aigu 329.63 Hz',
  command: (event: any) => {
      this.activeIndex = 5;
      this.messageService.add({severity:'info', summary:'Step 6', detail: event.item.label});
  }
},
{
    label: 'Finish',
    command: (event: any) => {
        this.activeIndex = 6;
        this.messageService.add({severity:'info', summary:'Félicitation! Votre instrument est prêt', detail: event.item.label});
    }
  }
  
  ];
  this.afficherValue(this.simulationFreq);
    // // début de méthode de recup fréquence
    // navigator.getUserMedia = navigator.getUserMedia;
    // const callback = stream => {
    //   var ctx = new AudioContext();
    //   var mic = ctx.createMediaStreamSource(stream);
    //   var analyser = ctx.createAnalyser();
    //   mic.connect(analyser);
    //   // osc.start(0);
    //   var data = new Uint8Array(analyser.frequencyBinCount);
    //   analyser.minDecibels = -45;

    //   const play = () => {
    //     analyser.getByteFrequencyData(data);
    //     // get fullest bin
    //     var idx = 0;
    //     for (var j = 0; j < analyser.frequencyBinCount; j++) {
    //       if (data[j] > data[idx]) {
    //         idx = j;
    //       }
    //     }
    //     let a = (idx * ctx.sampleRate) / analyser.fftSize;
    //     if (a < 336){
    //       this.frequency = a;
    //     }
    //     // this.zone.run(() => {});
    //     // requestAnimationFrame(play);
    //     return this.frequency;
    //   };
    //   // Themes begin
    //   am4core.useTheme(am4themes_animated);
    //   // Themes end

    //   // create chart
    //   var chart = am4core.create("chartdiv", am4charts.GaugeChart);
    //   chart.hiddenState.properties.opacity = 0; // this makes initial fade in effect

    //   chart.innerRadius = -25;

    //   var axis = chart.xAxes.push(new am4charts.ValueAxis<am4charts.AxisRendererCircular>());
    //   axis.min = 0;
    //   axis.max = 335;
    //   axis.strictMinMax = true;
    //   axis.renderer.grid.template.stroke = new am4core.InterfaceColorSet().getFor(
    //     "background"
    //   );
    //   axis.renderer.grid.template.strokeOpacity = 0.3;

    //   var range0 = axis.axisRanges.create();
    //   range0.value = 0;
    //   range0.endValue = 84;
    //   range0.axisFill.fillOpacity = 1;
    //   range0.axisFill.fill = this.colorSet[0];
    //   range0.axisFill.zIndex = -1;

    //   var range1 = axis.axisRanges.create();
    //   range1.value = 84;
    //   range1.endValue = 112;
    //   range1.axisFill.fillOpacity = 1;
    //   range1.axisFill.fill = this.colorSet[1]
    //   range1.axisFill.zIndex = -1;

    //   var range2 = axis.axisRanges.create();
    //   range2.value = 112;
    //   range2.endValue = 149;
    //   range2.axisFill.fillOpacity = 1;
    //   range2.axisFill.fill = this.colorSet[2]
    //   range2.axisFill.zIndex = -1;

    //   var range3 = axis.axisRanges.create();
    //   range3.value = 149;
    //   range3.endValue = 199;
    //   range3.axisFill.fillOpacity = 1;
    //   range3.axisFill.fill = this.colorSet[3]
    //   range3.axisFill.zIndex = -1;

    //   var range4 = axis.axisRanges.create();
    //   range4.value = 199;
    //   range4.endValue = 251;
    //   range4.axisFill.fillOpacity = 1;
    //   range4.axisFill.fill = this.colorSet[4]
    //   range4.axisFill.zIndex = -1;

    //   var range5 = axis.axisRanges.create();
    //   range5.value = 251;
    //   range5.endValue = 335;
    //   range5.axisFill.fillOpacity = 1;
    //   range5.axisFill.fill = this.colorSet[5]
    //   range5.axisFill.zIndex = -1;

    //   var hand = chart.hands.push(new am4charts.ClockHand());
    //   // using chart.setTimeout method as the timeout will be disposed together with a chart
    //   //chart.setTimeout(simmulationGT, 100);
    //   //function simmulationGT() {
    //   hand.showValue(this.simmulationFreq, 200, am4core.ease.cubicOut);
        //chart.setTimeout(simmulationGT, 100);
    //  }

    // };

    // navigator.getUserMedia(
    //   { video: false, audio: true },
    //   callback,
    //   console.log
    // );
   }
   
   activateStepTwo(){
       this.step1 = false; 
       this.step2 = true;
       this.step3 = false;
       this.step4 = false;
       this.step5 = false;
       this.step6 = false;
   }

   activateStepThree(){
    this.step1 = false; 
    this.step2 = false;
    this.step3 = true;
    this.step4 = false;
    this.step5 = false;
    this.step6 = false;
   }

   activateStepFour(){
    this.step1 = false; 
    this.step2 = false;
    this.step3 = false;
    this.step4 = true;
    this.step5 = false;
    this.step6 = false;
   }

   activateStepFive(){
    this.step1 = false; 
    this.step2 = false;
    this.step3 = false;
    this.step4 = false;
    this.step5 = true;
    this.step6 = false;
   }

   activateStepSix(){
    this.step1 = false; 
    this.step2 = false;
    this.step3 = false;
    this.step4 = false;
    this.step5 = false;
    this.step6 = true;
   }

   simulationStep1(){
       //simuler une frequence de step1 
       this.simulationFreq = 82.40;
       this.activeIndex = 1;
       this.step1Ok = true;
       this.activateStepTwo();
       this.afficherValue(this.simulationFreq);
   }

   simulationStep2(){
        //simuler une frequence de step2
        this.simulationFreq = 110;
        this.activeIndex = 2;
        this.step2Ok = true;
        this.activateStepThree();
        this.afficherValue(this.simulationFreq);
    }  

    simulationStep3(){
        //simuler une frequence de step3
        this.simulationFreq = 146.83;
        this.activeIndex = 3;
        this.step3Ok = true;
        this.activateStepFour();
        this.afficherValue(this.simulationFreq);
    }

    simulationStep4(){
        //simuler une frequence de step4
        this.simulationFreq = 196;
        this.activeIndex = 4;
        this.step4Ok = true;
        this.activateStepFive();
        this.afficherValue(this.simulationFreq);
    }

    simulationStep5(){
        //simuler une frequence de step5
        this.simulationFreq = 246.94;
        this.activeIndex = 5;
        this.step5Ok = true;
        this.activateStepSix();
        this.afficherValue(this.simulationFreq);
    }

    simulationStep6(){
        //simuler une frequence de step6
        this.simulationFreq = 329.63;
        this.activeIndex = 6;
        this.step1 = false; 
        this.step2 = false;
        this.step3 = false;
        this.step4 = false;
        this.step5 = false;
        this.step6 = false;
        this.step6Ok = true;
        this.accordagefinit = true;
        this.afficherValue(this.simulationFreq);
    }

    allIsReady(){
        this.route.navigate(['/home']);
        
    }

    afficherValue(value:number){
        // Themes begin
      am4core.useTheme(am4themes_animated);
      // Themes end

      // create chart
      var chart = am4core.create("chartdiv", am4charts.GaugeChart);
      chart.hiddenState.properties.opacity = 0; // this makes initial fade in effect

      chart.innerRadius = -25;

      var axis = chart.xAxes.push(new am4charts.ValueAxis<am4charts.AxisRendererCircular>());
      axis.min = 0;
      axis.max = 335;
      axis.strictMinMax = true;
      axis.renderer.grid.template.stroke = new am4core.InterfaceColorSet().getFor(
        "background"
      );
      axis.renderer.grid.template.strokeOpacity = 0.3;

      var range0 = axis.axisRanges.create();
      range0.value = 0;
      range0.endValue = 84;
      range0.axisFill.fillOpacity = 1;
      range0.axisFill.fill = this.colorSet[0];
      range0.axisFill.zIndex = -1;

      var range1 = axis.axisRanges.create();
      range1.value = 84;
      range1.endValue = 112;
      range1.axisFill.fillOpacity = 1;
      range1.axisFill.fill = this.colorSet[1]
      range1.axisFill.zIndex = -1;

      var range2 = axis.axisRanges.create();
      range2.value = 112;
      range2.endValue = 149;
      range2.axisFill.fillOpacity = 1;
      range2.axisFill.fill = this.colorSet[2]
      range2.axisFill.zIndex = -1;

      var range3 = axis.axisRanges.create();
      range3.value = 149;
      range3.endValue = 199;
      range3.axisFill.fillOpacity = 1;
      range3.axisFill.fill = this.colorSet[3]
      range3.axisFill.zIndex = -1;

      var range4 = axis.axisRanges.create();
      range4.value = 199;
      range4.endValue = 251;
      range4.axisFill.fillOpacity = 1;
      range4.axisFill.fill = this.colorSet[4]
      range4.axisFill.zIndex = -1;

      var range5 = axis.axisRanges.create();
      range5.value = 251;
      range5.endValue = 335;
      range5.axisFill.fillOpacity = 1;
      range5.axisFill.fill = this.colorSet[5]
      range5.axisFill.zIndex = -1;

      var hand = chart.hands.push(new am4charts.ClockHand());
      // using chart.setTimeout method as the timeout will be disposed together with a chart
      chart.setTimeout(simmulationGT, 100);
      function simmulationGT() {
        hand.showValue(value, 200, am4core.ease.cubicOut);
      }
    };

}
