import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from '@angular/router';


@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit {

  constructor(private spinner: NgxSpinnerService,
              private route:Router) { }

  ngOnInit() {
    /** spinner starts on init */
    this.spinner.show();
 
    setTimeout(() => {
      /** spinner ends after 8 seconds */
      this.spinner.hide();
      if(localStorage.getItem('id')){
        this.route.navigate(['/welcome']);
      }else{
        this.route.navigate(['/login']);
      }
      
    }, 2000);
  }
}
