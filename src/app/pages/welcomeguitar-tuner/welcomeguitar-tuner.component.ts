import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { Utilisateur } from 'src/app/modele/user';

@Component({
  selector: 'app-welcomeguitar-tuner',
  templateUrl: './welcomeguitar-tuner.component.html',
  styleUrls: ['./welcomeguitar-tuner.component.scss']
})
export class WelcomeguitarTunerComponent implements OnInit {

  idUser: string;
  UserConnected: Utilisateur;
  usernameUserConnected: string; 

  constructor(private userService: UserService,
              private route: Router) { }

  ngOnInit() {
    // //Recuperd le path actuel "welcome/{id}"
    // let currentPath = this.route.url;
    // this.idUser = currentPath.replace("/welcome/", "");

    //Recuperd l'id dans le localstorage 
    let idUserConnected = localStorage.getItem('id');
    console.log(idUserConnected);
    // this.userService.getInfoUser(this.idUser).subscribe(
    //   data => {
    //     this.UserConnected = new Utilisateur();
    //     this.UserConnected = data as Utilisateur;
    //     console.log(this.UserConnected);
    //     //Ecris dynamiquement le nom d'utilisateur
    //     this.usernameUserConnected = this.UserConnected.username;
    //     this.userService.setUniqueId(this.UserConnected[0].id);
    //   });
  }

  redirectGuitarTuner(){
    this.route.navigate(['/guitar-tuner/'+ this.idUser]);
  }

  redirectHome(){
    this.route.navigate(['/home/'+ this.idUser]);
  }

}
