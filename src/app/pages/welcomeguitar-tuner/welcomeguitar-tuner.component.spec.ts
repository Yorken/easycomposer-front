import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WelcomeguitarTunerComponent } from './welcomeguitar-tuner.component';

describe('WelcomeguitarTunerComponent', () => {
  let component: WelcomeguitarTunerComponent;
  let fixture: ComponentFixture<WelcomeguitarTunerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WelcomeguitarTunerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WelcomeguitarTunerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
