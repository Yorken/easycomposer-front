import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { InformationsComponent } from './pages/informations/informations.component';
import { GuitarTunerComponent } from './pages/guitar-tuner/guitar-tuner.component';
import { AccountSettingComponent } from './pages/account-setting/account-setting.component';
import { WelcomeguitarTunerComponent } from './components/welcomeguitar-tuner/welcomeguitar-tuner.component';



const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'informations', component: InformationsComponent},
  {path: 'guitar-tuner', component: GuitarTunerComponent},
  {path: 'my-account', component: AccountSettingComponent}, 
  {path: 'welcome', component: WelcomeguitarTunerComponent},
  {path: '', component: LoginComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
