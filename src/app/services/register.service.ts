import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Utilisateur } from '../modele/user';
import { MessageService } from './message.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  //Variable pour les requêtes 
  path = 'http://localhost:8080/utilisateurs/';
  user: Utilisateur;

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient) { }

  //Vérifie les mots de passe saisie directement dans le formulaire 
  concordanceMdp(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];
        const matchingControl = formGroup.controls[matchingControlName];

        if (matchingControl.errors && !matchingControl.errors.mustMatch) {
            console.log('Il y a des erreurs dans le formulaire');
            // return if another validator has already found an error on the matchingControl
            return;
        }

        // set error on matchingControl if validation fails
        if (control.value !== matchingControl.value) {
            //non concordance des mots de passes 
            console.log('non concordance des mots de passe ! ')
            matchingControl.setErrors({ mustMatch: true });
        } else {
            //concordance des mots de passes 
            console.log('mot de passes ok !')
            matchingControl.setErrors(null);
        }
    }
  }

  creerUser(user: Utilisateur ){
    //Envoi de la requête 
    // console.log("Sending request !");
    // console.log(this.path);
    // console.log(user);
    // console.log(JSON.stringify(user));
    return this.http.post(this.path, JSON.stringify(user), this.httpOptions);
    } 
  }


