import { TestBed } from '@angular/core/testing';

import { EcouteService } from './ecoute.service';

describe('EcouteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EcouteService = TestBed.get(EcouteService);
    expect(service).toBeTruthy();
  });
});
