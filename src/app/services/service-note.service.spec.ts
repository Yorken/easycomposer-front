import { TestBed } from '@angular/core/testing';

import { ServiceNoteService } from './service-note.service';

describe('ServiceNoteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServiceNoteService = TestBed.get(ServiceNoteService);
    expect(service).toBeTruthy();
  });
});
