import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Utilisateur } from '../modele/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  path: string = 'http://localhost:8080/utilisateurs/id/';
  pathUpdate: string = 'http://localhost:8080/utilisateurs';
  pathDelete: string = 'http://localhost:8080/utilisateurs/delete/';
  idUserConnected: string; 
  userConnected: Utilisateur;

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json',
                                'Access-Control-Allow-Origin':'*',
                               'Access-Control-Allow-Headers': '*'})
  };

  

  constructor(private http: HttpClient) { }

  getInfoUser(id: string){
    return this.http.get(this.path+ id );
  }

  setUniqueId(idUser: string){
    this.idUserConnected = idUser;
  }

  getUniqueid(){
    return this.idUserConnected;
  }

  signOut(){
    localStorage.removeItem('id');
  }

  changerEmail(newEmail:string){
      this.getInfoUser(localStorage.getItem('id')).subscribe(
      data => {
      this.userConnected = new Utilisateur();
      //Recuperd les infos de l'utilisateur 
      this.userConnected = data as Utilisateur;
      console.log(this.userConnected.email);
      this.userConnected.email = newEmail;
      console.log(this.userConnected.email);
      
    });
    //console.log(this.userConnected);
    return this.http.post(this.pathUpdate,JSON.stringify(this.userConnected), this.httpOptions);
  }

  changerUsername(newUserName: string){
      this.getInfoUser(localStorage.getItem('id')).subscribe(
      data => {
        this.userConnected = new Utilisateur();
      //Recuperd les infos de l'utilisateur 
        this.userConnected = data as Utilisateur;
      //console.log(this.userConnected);
        this.userConnected.username = newUserName;
      //console.log(this.userConnected.username);
    });
    //console.log(this.userConnected);
    return this.http.post(this.pathUpdate,JSON.stringify(this.userConnected), this.httpOptions);
  }

  changerPassword(newPassword:string){
    this.getInfoUser(localStorage.getItem('id')).subscribe(
      data => {
        this.userConnected = new Utilisateur();
        //Recuperd les infos de l'utilisateur 
        this.userConnected = data as Utilisateur;
        console.log(this.userConnected);
        console.log(newPassword);
        this.userConnected.password = newPassword;
        console.log(this.userConnected.password);
        //console.log(this.userConnected.username);
    });
    //console.log(this.userConnected);
    console.log(this.userConnected);
    return this.http.post(this.pathUpdate,JSON.stringify(this.userConnected), this.httpOptions);
  }

  giveFeedback(note: number, sexe: string, trancheAge: string, experience: string, commentaire: string){
    this.getInfoUser(localStorage.getItem('id')).subscribe(
      data => {
        this.userConnected = new Utilisateur();
        //Recuperd les infos de l'utilisateur 
        this.userConnected = data as Utilisateur;
        this.userConnected.note = note;
        this.userConnected.sexe = sexe;
        this.userConnected.trancheAge = trancheAge;
        this.userConnected.experience = experience;
        this.userConnected.commentaire = commentaire;
        
    });
    //console.log(this.userConnected);
    console.log(this.userConnected);
    return this.http.post(this.pathUpdate,JSON.stringify(this.userConnected), this.httpOptions);
  }

  deleteAccount(){
    let pathcomplet = this.pathDelete + localStorage.getItem('id');
    return this.http.delete(pathcomplet, this.httpOptions);
  }
  
}
