import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  
  path: string = 'http://localhost:8080/utilisateurs/email/';
  
  constructor(private http: HttpClient) { }

  verifierId(email: string){
    return this.http.get(this.path+email);
  }

}
