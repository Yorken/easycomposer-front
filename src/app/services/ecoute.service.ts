import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EcouteService {
  path: string;

  constructor(private http: HttpClient) { }

  getNoteByFrequence(frequence: number){
    let pathComplete = this.construirePath(frequence);
    return this.http.get(pathComplete);
  }

  construirePath(frequence: number){
    //Reset le path au début de la requête
    this.path = "";
    this.path = 'http://localhost:8080/notes/hz/';
    this.path = this.path + frequence;
    console.log(this.path);
    return this.path;
  }
  getGammeByFrequence(gamme: string, hz: any){
    let pathComplete = "";
    let note = "" + hz;
    //console.log('gamme :' + gamme + 'note : ' + note)
    pathComplete = 'http://localhost:8080/notes/gamme/hz/' + gamme + '/' + note;
    console.log(pathComplete);
    return this.http.get(pathComplete);
  }
}
