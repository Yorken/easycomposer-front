export class Historique {

    index: number;
    note: string;
    gamme: string;
    listeNotes: string;


    historique(index: number, note: string, gamme: string, listeNotes: string){
        this.index = index;
        this.note = note;
        this.gamme = gamme;
        this.listeNotes = listeNotes;
    }

   
}
