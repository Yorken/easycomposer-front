FROM node

WORKDIR /easycomposer/front/

COPY package.json /easycomposer/front

RUN npm install
RUN npm install -g @angular/cli

COPY . /easycomposer/front/

EXPOSE 4200:4200

CMD ["ng", "serve", "--host", "0.0.0.0"]
